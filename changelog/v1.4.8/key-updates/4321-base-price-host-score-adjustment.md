- Add `basePriceAdjustment` to the host score to check for `BaseRPCPrice` and
  `SectorAccessPrice` price violations.