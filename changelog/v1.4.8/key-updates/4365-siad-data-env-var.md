- Add SIAD_DATA_DIR environment variable which tells `siad` where to store the
  siad-specific data. This complements the SIA_DATA_DIR variable which tells 
  `siad` where to store general Sia data, such as the API password,
  configuration, etc.
