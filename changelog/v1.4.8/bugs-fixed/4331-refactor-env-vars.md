- Refactor the environment variables into the `build` package to address bug
  where `siac` and `siad` could be using different API Passwords.